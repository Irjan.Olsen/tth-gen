#include <stdio.h>
#include <string.h>
#include <ctype.h>

char block[4][4];
int total[4];

// Adds the letters's values to the running total
// and uses modulus on the total
void add_blk(char blk[4][4]) {
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			total[i] += block[j][i]-'A';
		}
		total[i] %= 26;
	}
}

// Reorders the letters in the given block according to description.
void reorder_blk(char blk[4][4]) {
	char tmp;
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < i+1; ++j)
		{
			tmp = block[i][0];
			block[i][0] = block[i][1];
			block[i][1] = block[i][2];
			block[i][2] = block[i][3];
			block[i][3] = tmp;
		}
	}

	tmp = block[3][0];
	block[3][0] = block[3][3];
	block[3][3] = tmp;
	tmp = block[3][1];
	block[3][1] = block[3][2];
	block[3][2] = tmp;
}

void proc_blk(char blk[4][4]) {
	add_blk(blk);
	reorder_blk(blk);
	add_blk(blk);
}

int ind = 0;
// Goes through the given string and gets only a-z and A-Z
// from the string. Stores the last position it was at in
// 'ind' for finding the next block easier.
void get_next_block(char const *text) {
	int len = 0;
	while (len < 16) {
		if (ind > strlen(text)) {
			block[len/4][len%4] = 'A';
			len++;
			ind--;
		}
		else if (isalpha(text[ind])) {
			block[len/4][len%4] = toupper(text[ind]);
			len++;
		}
		ind++;
	}
}

int main(int argc, char const *argv[])
{
	if (argc < 2) {
		printf("Need arguments!\n");
		return -1;
	}

	for (int i = 0; i < 4; ++i)	total[i] = 0;

	while (strlen(argv[1]) > ind) {
		get_next_block(argv[1]);
		proc_blk(block);
	}

	printf("(%i, %i, %i, %i)\n", total[0], total[1], total[2], total[3]);
	printf("(%c, %c, %c, %c)\n", total[0]+'A', total[1]+'A', total[2]+'A', total[3]+'A');
	return 0;
}