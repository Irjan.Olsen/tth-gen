# TTH-gen
A program that generates a TTH hash based on user input

## How to compile and run
Compile with `make` to create a `tth-gen` binary in the same directory. Uses gcc internally but works with clang.  

Run with `./tth-gen text` to encrypt `text` with the TTH algorithm.